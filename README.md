# Watch kqueue events with kwatch!

Usage:

```
kqueue event watcher

Usage:
  kwatch pid <pid>
  kwatch file [-rH] <file>
  kwatch --help
  kwatch --version

Options:
  -h --help         show this help
  -v --version      show version information
  -r --recursive    recurse through directories
  -H --dont-follow  follow symbolic links
```

Currently supports pids and files via kqueue
