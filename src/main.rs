extern crate colored;
extern crate docopt;
extern crate env_logger;
#[macro_use]
extern crate log;
extern crate rustc_serialize;
extern crate kqueue;
extern crate time;
extern crate walkdir;

use colored::*;
use docopt::Docopt;
use env_logger::LogBuilder;
use kqueue::{EventFilter, NOTE_DELETE, NOTE_WRITE, NOTE_EXTEND, NOTE_TRUNCATE, NOTE_ATTRIB,
             NOTE_LINK, NOTE_RENAME, NOTE_REVOKE, NOTE_EXIT, NOTE_FORK, NOTE_EXEC, NOTE_TRACK,
             NOTE_TRACKERR, Watcher};
use log::{LogLevel, LogLevelFilter, LogRecord};
use std::fmt::Display;
use std::path::Path;
use std::process::exit;
use walkdir::WalkDir;

static VERSION: &'static str = env!("CARGO_PKG_VERSION");
static USAGE: &'static str = "
kqueue event watcher

Usage:
  kwatch pid <pid>
  kwatch file [-rH] <file>
  kwatch --help
  kwatch --version

Options:
  -h --help         show this help
  -v --version      show version information
  -r --recursive    recurse through directories
  -H --dont-follow  follow symbolic links
";

#[derive(Debug, RustcDecodable)]
struct Args {
    arg_pid: i32,
    arg_file: String,
    flag_recursive: bool,
    flag_dont_follow: bool,
}

fn fatal<S: Display>(fmt: S) -> ! {
    error!("{}", fmt);
    exit(1);
}

fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.version(Some(VERSION.to_owned())).decode())
        .unwrap_or_else(|e| e.exit());

    let formatter = |record: &LogRecord| {
        match record.level() {
            LogLevel::Error => {
                let ret = format!("{}: {}", record.level(), record.args());
                format!("{}", ret.red().bold())
            }
            _ => {
                let color_now = format!("{}", time::now().ctime()).red();
                format!("{}: {}", color_now, record.args())
            }
        }
    };

    LogBuilder::new()
        .format(formatter)
        .filter(None, LogLevelFilter::Info)
        .init()
        .unwrap_or_else(|_| fatal("could not init logger"));

    let mut kq = Watcher::new()
        .unwrap_or_else(|e| fatal(format!("Could not create kq watcher: {}", e)));
    if args.arg_pid != 0 {
        let pid = args.arg_pid;
        kq.add_pid(pid,
                     EventFilter::EVFILT_PROC,
                     NOTE_EXIT | NOTE_FORK | NOTE_EXEC | NOTE_TRACK | NOTE_TRACKERR)
            .unwrap_or_else(|e| fatal(format!("Could not watch pid {}: {}", pid, e)));
    } else if args.arg_file != "" {
        let file = Path::new(&args.arg_file);
        add_file(&mut kq, &file);

        if args.flag_recursive {
            let walk =
                WalkDir::new(file).follow_links(!args.flag_dont_follow).min_depth(1).into_iter();
            for path in walk.filter_map(|e| e.ok()) {
                add_file(&mut kq, path.path());
            }
        }
    }

    kq.watch().unwrap_or_else(|e| fatal(format!("kqueue failed: {}", e)));
    for evt in kq.iter() {
        info!("{:?}", evt);
    }
}

fn add_file(kq: &mut Watcher, file: &Path) {
    kq.add_filename(&file,
                      EventFilter::EVFILT_VNODE,
                      NOTE_DELETE | NOTE_WRITE | NOTE_EXTEND | NOTE_TRUNCATE | NOTE_ATTRIB |
                      NOTE_LINK | NOTE_RENAME | NOTE_REVOKE)
        .unwrap_or_else(|e| fatal(format!("Could not watch file {:?}: {}", file, e)));
}
